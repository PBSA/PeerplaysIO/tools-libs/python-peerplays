known_chains = {
    "ALICE": {
        "chain_id": "6b6b5f0ce7a36d323768e534f3edb41c6d6332a541a95725b98e28d140850134",
        "core_symbol": "PPY",
        "prefix": "PPY",
    },
    "QAENV": {
        "chain_id": "7c1c72eb738b3ff1870350f85daca27e2d0f5dd25af27df7475fbd92815e421e",
        "core_symbol": "TEST",
        "prefix": "TEST"},
    "DEVNET": {
        "chain_id": "2c25aae5835fd54020329d4f150b04867e72cbd8f7f7b900a7c3da8a329a6014",
        "core_symbol": "TEST",
        "prefix": "TEST"},
    "TESTNET": {
        "chain_id": "195d4e865e3a27d2b204de759341e4738f778dd5c4e21860c7e8bf1bd9c79203",
        "core_symbol": "TEST",
        "prefix": "TEST"},
}
